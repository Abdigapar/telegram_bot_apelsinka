﻿using ApelsinkaBot.BLL.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyConnectionController : ControllerBase
    {
        private readonly ISurveyConnectionService _service;

        public SurveyConnectionController(ISurveyConnectionService service)
        {
            _service = service;
        }

        }
}
