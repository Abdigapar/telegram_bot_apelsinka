﻿using ApelsinkaBot.BLL.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyPointController : ControllerBase
    {
        private readonly ISurveyPointService _service;

        public SurveyPointController(ISurveyPointService service)
        {
            _service = service;
        }
    }
}
