﻿using ApelsinkaBot.BLL.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService _service;

        public NotificationController(INotificationService service)
        {
            _service = service;
        }

    }
}
