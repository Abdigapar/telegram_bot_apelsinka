﻿using ApelsinkaBot.BLL.Services;
using ApelsinkaBot.BLL.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace ApelsinkaBot.BLL.Configuration
{
    public static class ServiceConfiguration
    {
        public static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped<ISurveyService, SurveyService>();
            services.AddScoped<ISurveyConnectionService, SurveyConnectionService>();
            services.AddScoped<ISurveyPointService, SurveyPointService>();
        }
    }
}
