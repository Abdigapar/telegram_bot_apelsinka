﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services.Contracts
{
    public abstract class ServiceBase<R, T> : IServiceBase<T> where R : IBaseRepository<T>
                                                              where T : class
    {
        protected R _repository;
        protected ServiceBase(R repository)
        {
            _repository = repository;
        }

        public virtual Task<T> GetBySingleId(int id)
        {
            return _repository.GetBySingleId(id);
        }

        public virtual Task<List<T>> GetByMultipleIds(IEnumerable<int> id)
        {
            return _repository.GetByMultipleIds(id);
        }

        public virtual Task<List<T>> GetAll()
        {
            return _repository.GetAll();
        }

        public virtual Task Delete(T entity)
        {
            return _repository.Delete(entity);
        }

        public virtual Task Insert(T entity)
        {
            return _repository.Insert(entity);
        }

        public virtual Task Update(T entity)
        {
            return _repository.Update(entity);
        }
    }
}
