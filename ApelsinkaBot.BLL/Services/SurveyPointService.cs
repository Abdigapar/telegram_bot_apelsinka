﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services
{
    public class SurveyPointService : ServiceBase<ISurveyPointRepository, SurveyPoint>, ISurveyPointService
    {
        public SurveyPointService(ISurveyPointRepository repository) : base(repository)
        {
        }
    }
}
