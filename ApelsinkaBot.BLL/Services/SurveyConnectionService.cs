﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;

namespace ApelsinkaBot.BLL.Services
{
    /// <summary>
    /// Отвечает за работу с опросом.
    /// </summary>
    public class SurveyConnectionService : ServiceBase<ISurveyConnectionRepository, SurveyConnection>, ISurveyConnectionService
    {
        public SurveyConnectionService(ISurveyConnectionRepository repository) : base(repository)
        {
        }
    }
}
