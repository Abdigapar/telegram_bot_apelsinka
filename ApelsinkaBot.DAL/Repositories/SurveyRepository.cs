﻿using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories
{
    // Здесь логика работы с базой.
    public class SurveyRepository : BaseRepository<Survey>, ISurveyRepository
    {
        public override async Task<Survey> GetBySingleId(int id)
        {
            // Конструирование объекта Survey.
            // Пока просто строка из базы.
            return await db.Surveys.FindAsync(id);
        }
    }
}
