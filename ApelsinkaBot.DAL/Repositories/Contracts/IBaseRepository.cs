﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories.Contracts
{
    public interface IBaseRepository<T> where T : class
    {
        Task<T> GetBySingleId(int id);
        Task<List<T>> GetByMultipleIds(IEnumerable<int> id);
        Task<List<T>> GetAll();
        Task Insert(T entity);
        Task Update(T entity);
        Task Delete(T entity);
        Task SaveChange();
    }
}
