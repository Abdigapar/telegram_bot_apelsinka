﻿using ApelsinkaBot.DAL.Context;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories
{
    /// <summary>
    /// Методы в этом классе не менять. Логика в наследниках.
    /// </summary>
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected ApelsinkaDBContext db = new ApelsinkaDBContext();

        public virtual Task<T> GetBySingleId(int id)
        {
            throw new NotImplementedException();
        }

        public virtual Task<List<T>> GetByMultipleIds(IEnumerable<int> id)
        {
            throw new NotImplementedException();
        }

        public virtual Task<List<T>> GetAll()
        {
            throw new NotImplementedException();
        }

        public virtual Task Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual Task Insert(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual Task Update(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual Task SaveChange()
        {
            throw new NotImplementedException();
        }
    }
}
