﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ApelsinkaBot.DAL.Entities
{
    /// <summary>
    /// Соединяет 2 вопроса в опросе друг с другом.
    /// </summary>
    public class SurveyConnection
    {
        public SurveyPoint SurveyPoint { get; set; }
        [Column("survey_point_id")]
        public int SurveyPointId { get; set; }

        public SurveyPoint ParentSurveyPoint { get; set; }
        [Column("parent_id")]
        public int ParentSurveyPointId { get; set; }
    }
}
