﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Entities
{
    public class Notification
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        public Bot Bot { get; set; }
        [Column("bot_id")]
        public int BotId { get; set; }

        [Column("frequency")]
        public uint Frequency { get; set; }

        public Service Service { get; set; }
        [Column("service_id")]
        public int ServiceId { get; set; }
    }
}
