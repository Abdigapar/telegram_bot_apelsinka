﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Entities
{
    public class Bot
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("token")]
        public string Token { get; set; }

        public User User { get; set; }
        [Column("user_id")]
        public int UserId { get; set; }
    }
}
