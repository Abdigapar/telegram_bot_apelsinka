﻿using ApelsinkaBot.DAL.Repositories;
using ApelsinkaBot.DAL.Repositories.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace ApelsinkaBot.DAL.Configuration
{
    public static class RepositoryConfiguration
    {
        public static void ConfigureRepositories(this IServiceCollection services)
        {
            services.AddScoped<ISurveyRepository, SurveyRepository>();
            services.AddScoped<ISurveyConnectionRepository, SurveyConnectionRepository>();
            services.AddScoped<ISurveyPointRepository, SurveyPointRepository>();
        }
    }
}
